import { DataSource } from 'typeorm';
import { databaseConfiguration } from './database-config';

export const AppDataSourse = new DataSource(databaseConfiguration());
