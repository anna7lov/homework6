import { Router } from 'express';
import * as groupsController from './groups.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import { groupCreateSchema, groupUpdateSchema } from './schemas/group.schema';
import validator from '../application/middlewares/validation.middleware';
import { idParamSchema } from '../application/schemas/id-param.schema';
import checkGroupExists from './middlewares/group.middleware';

const router = Router();

router.get('/', controllerWrapper(groupsController.getAllGroupsWithStudents));

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupsController.getGroupWithStudentsById),
);

router.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup),
);

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(groupUpdateSchema),
  checkGroupExists,
  controllerWrapper(groupsController.updateGroupById),
);

router.delete(
  '/:id',
  validator.params(idParamSchema),
  checkGroupExists,
  controllerWrapper(groupsController.deleteGroupById),
);

export default router;
