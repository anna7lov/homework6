import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as groupsService from './groups.service';
import { IGroupCreateRequest } from './types/group-create-request.interface';
import { IGroupUpdateRequest } from './types/group-update-request.interface';

export const getAllGroupsWithStudents = async (
  request: Request,
  response: Response,
) => {
  const groups = await groupsService.getAllGroupsWithStudents();
  response.json(groups);
};

export const getGroupWithStudentsById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.getGroupWithStudentsById(id);
  response.json(group);
};

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = await groupsService.createGroup(request.body);
  response.status(201).json(group);
};

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  await groupsService.updateGroupById(id, request.body);
  response.sendStatus(204);
};

export const deleteGroupById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  await groupsService.deleteGroupById(id);
  response.sendStatus(204);
};
