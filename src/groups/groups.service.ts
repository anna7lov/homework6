import { DeleteResult, UpdateResult } from 'typeorm';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSourse } from '../configs/database/data-source';
import { Student } from '../students/entities/student.entity';
import { updateStudentsGroupToNull } from '../students/students.service';
import { Group } from './entities/group.entity';
import { IGroup } from './types/group.interface';

const groupsRepository = AppDataSourse.getRepository(Group);

export const findGroupById = async (groupId: number): Promise<Group | null> => {
  const group = await groupsRepository.findOne({
    where: { id: groupId },
  });
  return group;
};

export const getAllGroupsWithStudents = async (): Promise<
  (Group & { students: Student[] })[]
> => {
  const groups = await groupsRepository
    .createQueryBuilder('groups')
    .leftJoinAndSelect('groups.students', 'student')
    .getMany();

  return groups;
};

export const getGroupWithStudentsById = async (
  id: number,
): Promise<Group & { students: Student[] }> => {
  const group = await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .where('group.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return group;
};

export const createGroup = async (
  groupCreateSchema: Omit<IGroup, 'id'>,
): Promise<Group> => {
  return groupsRepository.save(groupCreateSchema);
};

export const updateGroupById = async (
  id: number,
  groupUpdateSchema: Partial<IGroup>,
): Promise<UpdateResult> => {
  const result = await groupsRepository.update(id, groupUpdateSchema);

  return result;
};

export const deleteGroupById = async (id: number): Promise<DeleteResult> => {
  await updateStudentsGroupToNull(id);

  const result = await groupsRepository.delete(id);

  return result;
};
