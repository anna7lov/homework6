import { Request, Response, NextFunction } from 'express';
import { HttpStatuses } from '../../application/enums/http-statuses.enum';
import HttpException from '../../application/exceptions/http-exception';
import { findGroupById } from '../groups.service';

const checkStudentExists = async (
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  try {
    const student = await findGroupById(+id);

    if (!student) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
    }

    next();
  } catch (error) {
    next(error);
  }
};

export default checkStudentExists;
