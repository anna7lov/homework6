import { IGroup } from './group.interface';
import { IStudent } from '../../students/types/student.interface';

export interface IGroupWithStudents extends IGroup {
  students: IStudent[];
}
