import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSourse } from '../configs/database/data-source';
import { Course } from '../courses/entities/course.entity';
import { Lector } from './entities/lector.entity';
import { ILector } from './types/lector.interface';

const lectorsRepository = AppDataSourse.getRepository(Lector);

export const findLectorById = async (
  lectorId: number,
): Promise<Lector | null> => {
  const lector = await lectorsRepository.findOne({
    where: { id: lectorId },
  });
  return lector;
};

export const getAllLectors = async (): Promise<Lector[]> => {
  return lectorsRepository.find({});
};

export const getLectorWithCoursesById = async (
  id: number,
): Promise<Lector & { courses: Course[] }> => {
  const lector = await lectorsRepository
    .createQueryBuilder('lector')
    .leftJoinAndSelect('lector.courses', 'course')
    .where('lector.id = :id', { id })
    .getOne();

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  return lector;
};

export const createLector = async (
  lectorCreateSchema: Omit<ILector, 'id'>,
): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: { email: lectorCreateSchema.email },
  });

  if (lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this email already exists',
    );
  }

  return lectorsRepository.save(lectorCreateSchema);
};
