import { Router } from 'express';
import * as lectorsController from './lectors.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { idParamSchema } from '../application/schemas/id-param.schema';
import { lectorCreateSchema } from './schemas/lectors.schema';

const router = Router();

router.get('/', controllerWrapper(lectorsController.getAllLectors));

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(lectorsController.getLectorWithCoursesById),
);

router.post(
  '/',
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorsController.createLector),
);

export default router;
