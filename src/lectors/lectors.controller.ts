import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as lectorsService from './lectors.service';
import { ILectorCreateRequest } from './types/lector-create-request.interface';

export const getAllLectors = async (request: Request, response: Response) => {
  const lectors = await lectorsService.getAllLectors();
  response.json(lectors);
};

export const getLectorWithCoursesById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorsService.getLectorWithCoursesById(id);
  response.json(lector);
};

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: Response,
) => {
  const lector = await lectorsService.createLector(request.body);
  response.status(201).json(lector);
};
