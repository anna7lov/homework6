import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Course } from '../../courses/entities/course.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'lector_course' })
export class LectorCourse {
  @PrimaryColumn({ name: 'lector_id' })
  lectorId: number;

  @PrimaryColumn({ name: 'course_id' })
  courseId: number;

  @ManyToOne(() => Lector, (lector) => lector.courses, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;

  @ManyToOne(() => Course, (course) => course.lectors, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;
}
