import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSourse } from '../configs/database/data-source';
import { findLectorById } from '../lectors/lectors.service';
import { Course } from './entities/course.entity';
import { ICourse } from './types/course.interface';

const coursesRepository = AppDataSourse.getRepository(Course);

export const findCourseById = async (
  courseId: number,
): Promise<Course | null> => {
  const course = await coursesRepository.findOne({
    where: { id: courseId },
  });
  return course;
};

export const getAllCourses = async (): Promise<Course[]> => {
  return coursesRepository.find({});
};

export const getCoursesByLectorId = async (
  lectorId: number,
): Promise<Course[]> => {
  const courses = await coursesRepository
    .createQueryBuilder('course')
    .leftJoin('course.lectors', 'lector')
    .where('lector.id = :lectorId', { lectorId })
    .getMany();

  return courses;
};

export const createCourse = async (
  courseCreateSchema: Omit<ICourse, 'id'>,
): Promise<Course> => {
  return coursesRepository.save(courseCreateSchema);
};

export const addLector = async (
  courseId: number,
  lectorId: number,
): Promise<void> => {
  const lector = await findLectorById(lectorId);

  if (!lector) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Lector with this id was not found',
    );
  }

  const isRelationExists = await coursesRepository
    .createQueryBuilder('course')
    .leftJoin('course.lectors', 'lector')
    .where('course.id = :courseId', { courseId })
    .andWhere('lector.id = :lectorId', { lectorId })
    .getCount();

  if (isRelationExists) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'This relation already exists',
    );
  }

  await coursesRepository
    .createQueryBuilder('course')
    .relation('lectors')
    .of(courseId)
    .add(lectorId);
};
