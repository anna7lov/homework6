import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';

export interface ICourseAddLectorRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: {
    lectorId: number;
  };
}
