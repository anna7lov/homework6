import Joi from 'joi';

export const lectorIdQuerySchema = Joi.object<{ lector_id: number }>({
  lector_id: Joi.number().optional(),
});
