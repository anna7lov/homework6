import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as coursesService from './courses.service';
import { ICourseCreateRequest } from './types/course-create-request.interface';
import { ICourseAddLectorRequest } from './types/course-add-lector.interface';

export const getCourses = async (
  request: Request<{ lector_id: number | undefined }>,
  response: Response,
) => {
  const lectorId = request.query.lector_id;
  if (!lectorId) {
    const courses = await coursesService.getAllCourses();
    response.json(courses);
  } else {
    const courses = await coursesService.getCoursesByLectorId(+lectorId);
    response.json(courses);
  }
};

export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: Response,
) => {
  const course = await coursesService.createCourse(request.body);
  response.status(201).json(course);
};

export const addLector = async (
  request: ValidatedRequest<ICourseAddLectorRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const { lectorId } = request.body;
  await coursesService.addLector(id, lectorId);
  response.sendStatus(204);
};
