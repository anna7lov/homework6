import { Request, Response, NextFunction } from 'express';
import { HttpStatuses } from '../../application/enums/http-statuses.enum';
import HttpException from '../../application/exceptions/http-exception';
import { findCourseById } from '../courses.service';

const checkCourseExists = async (
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  try {
    const course = await findCourseById(+id);

    if (!course) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
    }

    next();
  } catch (error) {
    next(error);
  }
};

export default checkCourseExists;
