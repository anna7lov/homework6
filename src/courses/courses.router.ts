import { Router } from 'express';
import * as coursesController from './courses.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { idParamSchema } from '../application/schemas/id-param.schema';
import {
  courseAddLectorSchema,
  courseCreateSchema,
} from './schemas/courses.schema';
import checkCourseExists from './middlewares/course.middleware';
import { lectorIdQuerySchema } from './schemas/lector_id.query.schema';

const router = Router();

router.get(
  '/',
  validator.query(lectorIdQuerySchema),
  controllerWrapper(coursesController.getCourses),
);

router.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse),
);

router.patch(
  '/:id/lector',
  validator.params(idParamSchema),
  validator.body(courseAddLectorSchema),
  checkCourseExists,
  controllerWrapper(coursesController.addLector),
);

export default router;
