import express from 'express';
import { Request, Response } from 'express';
import path from 'path';
import cors from 'cors';
import { AppDataSourse } from '../configs/database/data-source';
import logger from './middlewares/logger.middleware';
import bodyParser from 'body-parser';
import studentsRouter from '../students/students.router';
import exceptionsFilter from './middlewares/exceptions.filter';
import groupsRouter from '../groups/groups.router';
import lectorsRouter from '../lectors/lectors.router';
import coursesRouter from '../courses/courses.router';
import marksRouter from '../marks/marks.router';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);

AppDataSourse.initialize()
  .then(() => {
    console.log('Typeorm connected to database');
  })
  .catch((error) => {
    console.log('Error: ', error);
  });

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/lectors', lectorsRouter);
app.use('/api/v1/courses', coursesRouter);
app.use('/api/v1/marks', marksRouter);

app.use((request: Request, response: Response) => {
  response.status(404).json({
    status: 404,
    message: 'Not found',
  });
});

app.use(exceptionsFilter);

export default app;
