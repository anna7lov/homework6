import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as studentsService from './students.service';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { IStudentAddGroupRequest } from './types/student-add-group.interface';
import { ISearchByNameRequest } from './types/student-name.interface';

export const getStudentsWithGroupName = async (
  request: ValidatedRequest<ISearchByNameRequest>,
  response: Response,
) => {
  const { name } = request.query;
  const students = await studentsService.getStudentsWithGroupName(name);
  response.json(students);
};

export const getStudentWithGroupNameById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentWithGroupNameById(id);
  response.json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = await studentsService.createStudent(request.body);
  response.status(201).json(student);
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  await studentsService.updateStudentById(id, request.body);
  response.sendStatus(204);
};

export const addGroup = async (
  request: ValidatedRequest<IStudentAddGroupRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const { groupId } = request.body;
  await studentsService.addGroup(id, groupId);
  response.sendStatus(204);
};

export const deleteStudentById = async (
  request: Request<{ id: number }>,
  response: Response,
) => {
  const { id } = request.params;
  await studentsService.deleteStudentById(id);
  response.sendStatus(204);
};
