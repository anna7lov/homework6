import { Request, Response, NextFunction } from 'express';
import { HttpStatuses } from '../../application/enums/http-statuses.enum';
import HttpException from '../../application/exceptions/http-exception';
import { findStudentById } from '../students.service';

const checkStudentExists = async (
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  try {
    const student = await findStudentById(+id);

    if (!student) {
      throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
    }

    next();
  } catch (error) {
    next(error);
  }
};

export default checkStudentExists;
