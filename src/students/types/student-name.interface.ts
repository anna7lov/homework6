import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';

export interface ISearchByNameRequest extends ValidatedRequestSchema {
  [ContainerTypes.Query]: {
    name: string | undefined;
  };
}
