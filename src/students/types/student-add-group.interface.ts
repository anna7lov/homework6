import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './student.interface';

export interface IStudentAddGroupRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: {
    groupId: Exclude<Pick<IStudent, 'groupId'>['groupId'], null>;
  };
}
