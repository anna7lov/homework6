import { DeleteResult, UpdateResult } from 'typeorm';
import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { IStudent } from './types/student.interface';
import { AppDataSourse } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { findGroupById } from '../groups/groups.service';
import { removeMarks } from '../marks/marks.service';

const studentsRepository = AppDataSourse.getRepository(Student);

export const findStudentById = async (
  studentId: number,
): Promise<Student | null> => {
  const student = await studentsRepository.findOne({
    where: { id: studentId },
  });
  return student;
};

export const updateStudentsGroupToNull = async (
  groupId: number,
): Promise<void> => {
  const studentsInGroup = await studentsRepository.find({
    where: { groupId },
  });

  if (studentsInGroup.length > 0) {
    await studentsRepository
      .createQueryBuilder()
      .update()
      .set({ groupId: null })
      .where('groupId = :groupId', { groupId })
      .execute();
  }
};

export const getStudentsWithGroupName = async (
  name: string | undefined,
): Promise<(Omit<Student, 'groupId'> & { groupName: string })[]> => {
  const queryBuilder = studentsRepository
    .createQueryBuilder('students')
    .select([
      'students.id as id',
      'students.createdAt as "createdAt"',
      'students.updatedAt as "updatedAt"',
      'students.name as name',
      'students.surname as surname',
      'students.email as email',
      'students.age as age',
      'students.imagePath as "imagePath"',
    ])
    .leftJoin('students.group', 'group')
    .addSelect('group.name as "groupName"');

  if (name) {
    queryBuilder.where('students.name = :name', { name });
  }

  const students = await queryBuilder.getRawMany();

  return students;
};

export const getStudentWithGroupNameById = async (
  id: number,
): Promise<Omit<Student, 'groupId'> & { groupName: string }> => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.createdAt as "createdAt"',
      'student.updatedAt as "updatedAt"',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as "imagePath"',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return student;
};

export const createStudent = async (
  studentCreateSchema: Omit<IStudent, 'id'>,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: { email: studentCreateSchema.email },
  });

  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this email already exists',
    );
  }

  return studentsRepository.save(studentCreateSchema);
};

export const updateStudentById = async (
  id: number,
  studentUpdateSchema: Partial<IStudent>,
): Promise<UpdateResult> => {
  if (studentUpdateSchema.groupId) {
    const group = await findGroupById(studentUpdateSchema.groupId);

    if (!group) {
      throw new HttpException(
        HttpStatuses.NOT_FOUND,
        'Group with this id was not found',
      );
    }
  }

  const result = await studentsRepository.update(id, studentUpdateSchema);

  return result;
};

export const addGroup = async (
  studentId: number,
  groupId: number,
): Promise<UpdateResult> => {
  const group = await findGroupById(groupId);

  if (!group) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Group with this id was not found',
    );
  }

  const studentWithGroup = await studentsRepository.update(studentId, {
    groupId,
  });

  return studentWithGroup;
};

export const deleteStudentById = async (id: number): Promise<DeleteResult> => {
  await removeMarks(id);

  const result = await studentsRepository.delete(id);

  return result;
};
