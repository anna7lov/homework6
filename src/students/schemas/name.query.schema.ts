import Joi from 'joi';

export const nameQuerySchema = Joi.object<{ name: string }>({
  name: Joi.string().optional(),
});
