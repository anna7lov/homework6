import { Router } from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import {
  studentAddGroupSchema,
  studentCreateSchema,
  studentUpdateSchema,
} from './schemas/student.schema';
import validator from '../application/middlewares/validation.middleware';
import { idParamSchema } from '../application/schemas/id-param.schema';
import checkStudentExists from './middlewares/student.middleware';
import { nameQuerySchema } from './schemas/name.query.schema';

const router = Router();

router.get(
  '/',
  validator.query(nameQuerySchema),
  controllerWrapper(studentsController.getStudentsWithGroupName),
);

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentWithGroupNameById),
);

router.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  checkStudentExists,
  controllerWrapper(studentsController.updateStudentById),
);

router.patch(
  '/:id/group',
  validator.params(idParamSchema),
  validator.body(studentAddGroupSchema),
  checkStudentExists,
  controllerWrapper(studentsController.addGroup),
);

router.delete(
  '/:id',
  validator.params(idParamSchema),
  checkStudentExists,
  controllerWrapper(studentsController.deleteStudentById),
);

export default router;
