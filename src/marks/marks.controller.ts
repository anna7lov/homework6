import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as marksService from './marks.service';
import { IMarkAddRequest } from './types/mark-add-request.interface';

export const getMarks = async (
  request: Request<{ student_id: number } | { course_id: number }>,
  response: Response,
) => {
  const studentId = request.query.student_id;
  const courseId = request.query.course_id;

  if (studentId) {
    const studentMarks = await marksService.getStudentMarks(+studentId);
    response.json(studentMarks);
  } else if (courseId) {
    const courseMarks = await marksService.getCourseMarks(+courseId);
    response.json(courseMarks);
  }
};

export const addMark = async (
  request: ValidatedRequest<IMarkAddRequest>,
  response: Response,
) => {
  const createdMark = await marksService.addMark(request.body);
  response.status(201).json(createdMark);
};
