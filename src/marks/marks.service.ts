import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSourse } from '../configs/database/data-source';
import { findCourseById } from '../courses/courses.service';
import {
  findLectorById,
  getLectorWithCoursesById,
} from '../lectors/lectors.service';
import { findStudentById } from '../students/students.service';
import { Mark } from './entities/mark.entity';
import { IMark } from './types/mark.interface';

const marksRepository = AppDataSourse.getRepository(Mark);

export const removeMarks = async (studentId: number): Promise<void> => {
  const studentMarks = await marksRepository.find({
    where: { student: { id: studentId } },
  });
  if (studentMarks.length > 0) {
    await marksRepository.remove(studentMarks);
  }
};

export const getStudentMarks = async (
  studentId: number,
): Promise<(Pick<Mark, 'mark'> & { courseName: string })[]> => {
  const student = await findStudentById(studentId);

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  const marks = await marksRepository
    .createQueryBuilder('mark')
    .select(['course.name AS "courseName"', 'mark.mark AS mark'])
    .leftJoin('mark.course', 'course')
    .where('mark.studentId = :studentId', { studentId })
    .getRawMany();

  return marks;
};

export const getCourseMarks = async (
  courseId: number,
): Promise<
  (Pick<Mark, 'mark'> & { courseName: string } & { lectorName: string } & {
    studentName: string;
  })[]
> => {
  const course = await findCourseById(courseId);

  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  const marks = await marksRepository
    .createQueryBuilder('mark')
    .select([
      'course.name AS "courseName"',
      'lector.name AS "lectorName"',
      'student.name AS "studentName"',
      'mark.mark AS mark',
    ])
    .leftJoin('mark.course', 'course')
    .leftJoin('mark.lector', 'lector')
    .leftJoin('mark.student', 'student')
    .where('course.id = :courseId', { courseId })
    .getRawMany();

  return marks;
};

export const addMark = async (
  markAddSchema: Omit<IMark, 'id'>,
): Promise<Mark> => {
  const course = await findCourseById(markAddSchema.courseId);
  const student = await findStudentById(markAddSchema.studentId);
  const lector = await findLectorById(markAddSchema.lectorId);

  if (!course || !student || !lector) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Course, Student or Lector not found',
    );
  }

  const lectorWithCourses = await getLectorWithCoursesById(
    markAddSchema.lectorId,
  );

  const lectorTeachesCourse = lectorWithCourses.courses.some(
    (course) => course.id === markAddSchema.courseId,
  );

  if (!lectorTeachesCourse) {
    throw new HttpException(
      HttpStatuses.FORBIDDEN,
      'Lector is not assigned to teach this course',
    );
  }
  return marksRepository.save(markAddSchema);
};
