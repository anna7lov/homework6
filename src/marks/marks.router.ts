import { Router } from 'express';
import * as marksController from './marks.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { markAddSchema } from './schemas/marks.schema';
import { idQuerySchema } from './schemas/id.query.schema';

const router = Router();

router.get(
  '/',
  validator.query(idQuerySchema),
  controllerWrapper(marksController.getMarks),
);

router.post(
  '/',
  validator.body(markAddSchema),
  controllerWrapper(marksController.addMark),
);

export default router;
