import Joi from 'joi';

export const idQuerySchema = Joi.object<{
  student_id: number;
  course_id: number;
}>({
  student_id: Joi.number(),
  course_id: Joi.number(),
}).custom((value, helpers) => {
  const { student_id, course_id } = value;

  if ((!student_id && !course_id) || (student_id && course_id)) {
    return helpers.message({
      custom: 'Query must contain student_id or course_id',
    });
  }

  return value;
});
