import Joi from 'joi';
import { IMark } from '../types/mark.interface';

export const markAddSchema = Joi.object<Omit<IMark, 'id'>>({
  mark: Joi.number().min(0).max(10).required(),
  courseId: Joi.number().required(),
  studentId: Joi.number().required(),
  lectorId: Joi.number().required(),
});
